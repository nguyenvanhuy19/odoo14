# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import _, api, fields, models
from odoo.exceptions import UserError, ValidationError
import ast


class WizardFinallySpreadsheet(models.TransientModel):
    _name = 'finally.spreadsheet'
    _description = 'Finally Spreadsheet'

    document_ids = fields.Many2many(comodel_name='documents.document', relation='finally_spreadsheet_document_rel',
                                    column1='wizard_id', column2='document_id', string='Documents')

    def action_confirm(self):
        plan_id = 0
        if len(self.document_ids) != 1:
            raise UserError('Bạn chỉ được chọn duy nhất 1 bản ghi để Hoàn thành!')
        for document_id in self.document_ids:
            document_id.is_done = True
            plan_id = document_id.plan_month_id.id
            if document_id.plan_month_id.state != 'done':
                document_id.plan_month_id.state = 'done'

        action = {
            'name': _('Plan Production'),
            'view_mode': 'tree,form',
            'res_model': 'mrp.plan.month',
            'type': 'ir.actions.act_window',
            'domain': [('id', '=', plan_id)],
        }
        return action

    def _action_generate_mrp_production(self, vals_sequence, document_id):
        Product = self.env['product.product']
        MrpProduction = self.env['mrp.production']
        Bom = self.env['mrp.bom']
        for key, val_sequence in vals_sequence.items():
            for vals in val_sequence:
                if vals:
                    for lot, spec in vals.items():
                        product_id = Product.search([('id', '=', spec)])
                        date_planned_start = document_id.plan_month_id.date_from.replace(day=key)
                        if not product_id:
                            raise ValidationError(_("SPEC có ID là '%s' đã bị xóa hoặc không tồn tại!!", str(spec)))
                        bom_id = Bom.search([('product_id', '=', product_id.id), ('type', '=', 'normal')], limit=1)
                        if not bom_id:
                            raise ValidationError(_("SPEC: '%s' chưa cấu hình BOM. Vui lòng kiểm tra lại!!", str(product_id.display_name)))
                        mo = MrpProduction.create({
                            'product_id': product_id.id,
                            'product_uom_id': product_id.uom_id.id,
                            'company_id': self.env.company.id,
                            'production_lot': lot,
                            # 'product_qty': 30,
                            'bom_id': bom_id.id,
                            'document_id': document_id.id,
                            'plan_month_id': document_id.plan_month_id.id,
                            'date_planned_start': date_planned_start
                        })
                        mo._onchange_bom_id()
                        mo.product_qty = 30
                        mo._onchange_move_finished()
                        mo._onchange_move_raw()
                        mo._onchange_workorder_ids()

