# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class SaleManagement(models.Model):
    _name = 'sale.management'
    _inherit = 'order.management'
    _rec_name = 'project_name'

    active = fields.Boolean(string="Bool", default=False)

    @api.model
    def create(self, vals_list):
        res = super(SaleManagement, self).create(vals_list)
        if res.project_name != False:
            return res

