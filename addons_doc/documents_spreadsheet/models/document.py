# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api
from odoo.tools import image_process


class Document(models.Model):
    _inherit = 'documents.document'

    handler = fields.Selection([('spreadsheet', 'Spreadsheet')], ondelete={'spreadsheet': 'cascade'})
    raw = fields.Binary(related='attachment_id.raw', readonly=False)
    plan_month_id = fields.Many2one(comodel_name='mrp.plan.month', string='Plan Production Month')
    is_approve = fields.Boolean(string="Plan to approve", default=False)
    is_done = fields.Boolean(string="Plan done", default=False)
    plan_name = fields.Char(string="Plan Name", required=False, )
    vals_spec_qty = fields.Char(string="Spec Quantity Per Day of Month", required=False, )
    vals_sequence = fields.Char(string="Sequence Lot and Quantity of Month", required=False, )
    back_up = fields.Char(string="Back up")
    index_color = fields.Char(string='Color Index')

    # TODO extend the versioning system to use raw.

    @api.model_create_multi
    def create(self, vals_list):
        false = False
        true = True
        if not vals_list[0].get('plan_month_id', False):
            default_folder = self.env.ref('documents_spreadsheet.documents_spreadsheet_folder', raise_if_not_found=False)
            if not default_folder:
                default_folder = self.env['documents.folder'].search([], limit=1, order="sequence asc")
        else:
            default_folder = self.env['documents.folder'].search([('plan_month_id', '=', vals_list[0]['plan_month_id'])], limit=1, order="sequence asc")
            if not default_folder:
                plan_name = vals_list[0]['plan_name']
                default_folder = self.env['documents.folder'].create({
                    'name': plan_name,
                    'plan_month_id': vals_list[0]['plan_month_id']
                })

        for vals in vals_list:
            if vals.get('handler') == 'spreadsheet':
                # vals['folder_id'] = vals.get('folder_id', default_folder.id)
                vals['folder_id'] = default_folder.id
                if 'thumbnail' in vals:
                    vals['thumbnail'] = image_process(vals['thumbnail'], size=(80, 80), crop='center')
        documents = super().create(vals_list)
        for document in documents:
            if document.handler == 'spreadsheet':
                self.env['spreadsheet.contributor']._update(self.env.user, document)
        return documents

    def write(self, vals):
        for document in self:
            if 'raw' in vals and document.handler == 'spreadsheet':
                self.env['spreadsheet.contributor']._update(self.env.user, document)
        return super().write(vals)

    @api.depends('checksum', 'handler')
    def _compute_thumbnail(self):
        # Spreadsheet thumbnails cannot be computed from their binary data.
        # They should be saved independently.
        spreadsheets = self.filtered(lambda d: d.handler == 'spreadsheet')
        super(Document, self - spreadsheets)._compute_thumbnail()

    @api.model
    def get_spreadsheets_to_display(self):
        Contrib = self.env["spreadsheet.contributor"]
        visible_docs = self.search([("handler", "=", "spreadsheet")])
        contribs = Contrib.search(
            [
                ("document_id", "in", visible_docs.ids),
                ("user_id", "=", self.env.user.id),
            ], order="last_update_date desc"
        )
        user_docs = contribs.document_id
        # keep only visible docs, but preserve order of contribs
        return (user_docs | visible_docs).read(["name"])
