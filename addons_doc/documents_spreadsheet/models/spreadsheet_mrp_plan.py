# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import ast
import difflib
import re
from odoo import fields, models, api
import json

LETTERS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
def excel_style(row, col):
    """ Convert given row and column number to an Excel-style cell name. """
    result = []
    while col:
        col, rem = divmod(col - 1, 26)
        result[:0] = LETTERS[rem]
    return ''.join(result) + str(row)

def column_string_to_num(s):
    n = ord(s[-1]) - 64
    if s[:-1]:
        return 26 * (column_string_to_num(s[:-1])) + n
    else:
        return n

def get_change_dict(change):
    change_dict = {}
    i = 0
    count = len(change)
    while i < len(change):
        if change[i][1].isnumeric() == True:
            if i == count - 1:
                change_dict[change[i][0]] = change[i][1]
                break
            change_dict[change[i][0]] = change[i][1]
            if (change[i + 1][0] - change[i][0]) == 1:
                change_dict[change[i][0]] = change_dict[change[i][0]] + change[i + 1][1]
                i = i + 1
        i = i + 1
    change_lst = []
    for key, value in change_dict.items():
        change_lst.append([key, value])
    return change_lst

def get_change_val(new_raw, change_val):
    for i in change_val:
        val_str = new_raw[(i[0]-29):i[0]+2].replace('{', '').replace('}', '')
        val = re.findall(r'\b,".+":"content\b', val_str)[-1:]
        val2 = re.findall(r'\bcontent":"[0-9]+', val_str)[-1:]
        if val:
            i[0] = val[0][2:6].replace('"', '').replace(':', '')
        if val2:
            i[1] = val2[0][10:]
    return change_val

def rewrite_change_val(change_val_dict, val_origin, check):
    for change in change_val_dict:
        if check:
            change_val = change[1]
        else:
            change_val = 0
        for day in val_origin:
            flag = False
            key = next(iter(val_origin[day]))
            if type(val_origin[day][key]) is not dict:
                continue
            if next(iter(val_origin[day][key]))[0] != change[0][0]:
                continue
            for val in val_origin[day]:
                val_key = next(iter(val_origin[day][val]))
                if val_key == change[0]:
                    val_origin[day][val][val_key] = change_val
                    flag = True
                    break
            if flag:
                break
    return val_origin

def rewrite_change_val_seq(change_val_dict, val_seq_origin, check, index_color):
    for change in change_val_dict:
        if check:
            check_quan = int(change[1])
        else:
            check_quan = 0

        if change[0][:2].isalpha():
            check_char = change[0][:2]
            index = change[0][2:]
        else:
            check_char = change[0][0]
            index = change[0][1:]

        for day in val_seq_origin:
            flag = False
            for val in val_seq_origin[day]:
                val_key = next(iter(val))
                if val_key.startswith(check_char) and val_key[len(check_char):].isnumeric() == True:
                    spec_key = next(iter(val[val_key]))
                    spec_id = next(iter(val[val_key][spec_key]))
                    colors = val[val_key][spec_key][spec_id]

                    if val_key == change[0]:
                        key = index_color[index]
                        if key in colors:
                            colors[key] = check_quan
                        flag = True
                        break
                    else:
                        colors.update({index_color[index]: check_quan})
                        flag = True
                        break
            if flag:
                break
    return val_seq_origin

def get_add_delete(sheet, new_sheet):
    add = []
    delete = []
    for i, s in enumerate(difflib.ndiff(sheet, new_sheet)):
        if s[0] == ' ':
            continue
        elif s[0] == '+':
            add.append((i, s[-1]))
        elif s[0] == '-':
            delete.append((i, s[-1]))
    return add, delete

class Document(models.Model):
    _inherit = 'documents.document'

    def write(self, vals):
        for document in self:
            if document.plan_month_id.id and vals.get('raw'):
                decode_old = self.raw.decode('utf-8')
                decode_old = decode_old.replace(' ', '').replace(',"style":0', '')
                sheet_lst = re.split(r',"conditionalFormats":', decode_old)
                first_sheet = re.split(r'"cells":', sheet_lst[0])[1]
                second_sheet = re.split(r'"cells":', sheet_lst[1])[1]

                new = vals['raw'].replace(' ', '')
                new_sheet_lst = re.split(r',"conditionalFormats":', new)
                new_first_sheet = re.split(r'"cells":', new_sheet_lst[0])[1]
                new_second_sheet = re.split(r'"cells":', new_sheet_lst[1])[1]

                # Change first sheet
                if first_sheet != new_first_sheet:
                    add, delete = get_add_delete(first_sheet, new_first_sheet)
                    if add or delete:
                        change_val = get_change_dict(add)
                        del_change_val = get_change_dict(delete)
                        change_val_dict = get_change_val(new_first_sheet, change_val)
                        del_change_val_dict = get_change_val(new_first_sheet, del_change_val)
                        for var in del_change_val_dict:
                            if var in change_val_dict:
                                del_change_val_dict.remove(var)

                        val_origin = ast.literal_eval(self.vals_spec_qty)
                        if change_val_dict:
                            val_origin = rewrite_change_val(change_val_dict, val_origin, True)
                        if del_change_val_dict:
                            val_origin = rewrite_change_val(del_change_val_dict, val_origin, False)

                        new_val_origin = json.dumps(val_origin)
                        self.write({
                            'vals_spec_qty': new_val_origin
                        })

                # Change second sheet
                if second_sheet != new_second_sheet:
                    add, delete = get_add_delete(second_sheet, new_second_sheet)
                    if add or delete:
                        change_val = get_change_dict(add)
                        del_change_val = get_change_dict(delete)
                        change_val_dict = get_change_val(new_second_sheet, change_val)
                        del_change_val_dict = get_change_val(new_second_sheet, del_change_val)
                        for var in del_change_val_dict:
                            if var in change_val_dict:
                                del_change_val_dict.remove(var)

                        val_seq_origin = ast.literal_eval(self.vals_sequence)
                        index_color = ast.literal_eval(self.index_color)
                        if change_val_dict:
                            val_seq_origin = rewrite_change_val_seq(change_val_dict, val_seq_origin, True, index_color)
                        if del_change_val_dict:
                            val_seq_origin = rewrite_change_val_seq(del_change_val_dict, val_seq_origin, False, index_color)

                        new_val_seq_origin = json.dumps(val_seq_origin)
                        self.write({
                            'vals_sequence': new_val_seq_origin
                        })

        return super(Document, self).write(vals)

    def _count_spec(self, document):
        spec_checked = []
        count_spec = 0
        for line_id in document.plan_month_id.line_ids:
            if line_id.product_id.id not in spec_checked:
                spec_checked.append(line_id.product_id.id)
                count_spec += 1
        return count_spec

    def _count_day(self, document):
        count_day = 0
        if document.plan_month_id.date_from and document.plan_month_id.date_to:
            count_day = (document.plan_month_id.date_to - document.plan_month_id.date_from).days + 1
        return count_day