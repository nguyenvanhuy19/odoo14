# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import _, api, fields, models
from odoo.exceptions import UserError


class WizardApproveSpreadsheet(models.TransientModel):
    _name = 'approve.spreadsheet'
    _description = 'Approve Spreadsheet'

    document_ids = fields.Many2many(comodel_name='documents.document', relation='spreadsheet_document_rel', column1='wizard_id', column2='document_id', string='Documents')

    def action_confirm(self):
        plan_id = 0
        for document_id in self.document_ids:
            document_id.is_approve = True
            plan_id = document_id.plan_month_id.id
            if document_id.plan_month_id.state != 'to_approve':
                document_id.plan_month_id.state = 'to_approve'

        action = {
            'name': _('Plan Production'),
            'view_mode': 'tree,form',
            'res_model': 'mrp.plan.month',
            'type': 'ir.actions.act_window',
            'domain': [('id', '=', plan_id)],
        }
        return action
